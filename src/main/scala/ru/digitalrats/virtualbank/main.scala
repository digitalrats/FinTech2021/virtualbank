package ru.digitalrats.virtualbank

import cats.data.Kleisli
import cats.effect.{ExitCode, IO, IOApp}
import org.http4s.{Request, Response}
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.server.Router
import ru.digitalrats.virtualbank.dao.Repo
import ru.digitalrats.virtualbank.routes.BankRoutes

object main extends IOApp {
  val httpRoutes: Kleisli[IO, Request[IO], Response[IO]] = Router[IO](
    "/" -> BankRoutes.routes(Repo)
  ).orNotFound

  override def run(args: List[String]): IO[ExitCode] = {

    BlazeServerBuilder[IO]
      .bindHttp(4242, "0.0.0.0")
      .withHttpApp(httpRoutes)
      .serve
      .compile
      .drain
      .as(ExitCode.Success)
  }


}

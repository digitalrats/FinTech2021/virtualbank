package ru.digitalrats.virtualbank.routes

import cats.effect.IO
import com.typesafe.scalalogging.StrictLogging
import io.circe.generic.auto._
import org.http4s.circe.jsonOf
import org.http4s.dsl.Http4sDsl
import org.http4s.{EntityDecoder, HttpRoutes}
import ru.digitalrats.virtualbank.algebras.BusinessAlg

object BankRoutes extends StrictLogging {
  def routes(implicit repo: BusinessAlg[IO]): HttpRoutes[IO] = {
    case class TransferRequest(from: Int, to: Int, amount:Double)

    implicit val transferDecoder: EntityDecoder[IO, TransferRequest] = jsonOf[IO, TransferRequest]

    val dsl = new Http4sDsl[IO]{}
    import dsl._

    HttpRoutes.of[IO] {
      case req @ POST -> Root / "transfer" =>
        (for {
          req <- req.as[TransferRequest]
          resp <- repo.transfer(req.from,req.to, req.amount)
        } yield resp).attempt.flatMap {
          case Left(err:Throwable) => BadRequest(err.getMessage)
          case Right(_) => Ok("Done")
        }
      case req @ POST -> Root / "validate" =>
        (for {
          req <- req.as[TransferRequest]
          resp <- repo.hasAmount(req.from, req.amount)
        } yield resp).map(
          rsp => {
            logger.debug(s"Validation resp $rsp")
            if (rsp) Ok() else BadRequest("Insufficient")
          })
          .attempt.flatMap {
          case Left(err:Throwable) => BadRequest(err.getMessage)
          case Right(rsp) => rsp
        }
    }
  }

}

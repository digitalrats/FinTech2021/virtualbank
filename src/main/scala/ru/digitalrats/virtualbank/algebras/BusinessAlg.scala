package ru.digitalrats.virtualbank.algebras

trait BusinessAlg[F[_]] {
  def transfer(fromId: Int, to: Int, amount: Double): F[Unit]
  def hasAmount(id: Int, amount: Double): F[Boolean]
}

package ru.digitalrats.virtualbank

import cats.effect._
import cats.implicits._
import doobie._
import doobie.implicits._
import doobie.h2._

import ru.digitalrats.virtualbank.algebras.BusinessAlg

package object dao {

  implicit object Repo extends BusinessAlg[IO] {
    private val transactor: Resource[IO, H2Transactor[IO]] =
      for {
        ce <- ExecutionContexts.fixedThreadPool[IO](32) // our connect EC
        xa <- H2Transactor.newH2Transactor[IO](
          "jdbc:h2::file:target/bank.h2;DB_CLOSE_DELAY=-1", // connect URL
          "",                                     // username
          "",                                     // password
          ce,                                     // await connection here
        )
      } yield xa

    override def transfer(fromId: Int, toId: Int, amount: Double): IO[Unit] = {
      transactor.use { xa =>
        for {
          amntFrom <- sql"select amount from accounts where id=$fromId".query[Int].unique.transact(xa)
          amntTo <- sql"select amount from accounts where id=$toId".query[Int].unique.transact(xa)
          amm = if (amntFrom >= amount) (amntFrom - amount, amntTo + amount)
          else throw new Exception("Insufficient")
          _ <- sql"update accounts set amount = ${amm._1} where id=$fromId".update.run.transact(xa)
          _ <- sql"update accounts set amount = ${amm._2} where id=$toId".update.run.transact(xa)
        } yield ()
      }
    }

    override def hasAmount(id: Int, amount: Double): IO[Boolean] = {
      transactor.use { xa =>
        for {
          amnt <- sql"select amount from accounts where id=$id".query[Int].unique.transact(xa)
        } yield (amnt >= amount)
      }
    }
  }

}

CREATE TABLE accounts (
    id INT NOT NULL PRIMARY KEY,
    amount DOUBLE
);

INSERT INTO accounts (id,amount) VALUES (0,42000);
INSERT INTO accounts (id,amount) VALUES (1,42000);
name := "virtualbank"

version := "0.0.1"

scalaVersion := "2.13.6"

libraryDependencies ++= Seq(

  // Start with this one
  "org.tpolecat" %% "doobie-core"      % "1.0.0-RC1",

  // And add any of these as needed
  "org.tpolecat" %% "doobie-h2"        % "1.0.0-RC1",
)

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-dsl",
  "org.http4s" %% "http4s-blaze-server",
  "org.http4s" %% "http4s-blaze-client",
  "org.http4s" %% "http4s-circe",
).map(_ % "1.0.0-M25")

libraryDependencies ++= Seq(
  "io.circe" %% "circe-generic",
  // Optional for string interpolation to JSON model
  "io.circe" %% "circe-literal"
).map(_ % "0.14.1")

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.5"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4"

enablePlugins(FlywayPlugin)

libraryDependencies += "com.h2database" % "h2" % "1.4.200"

flywayUrl := "jdbc:h2::file:target/bank.h2"
flywayUser := ""
flywayPassword := ""
flywayLocations += "db/migration"
Test / flywayUrl := "jdbc:h2:file:target/bank.h2"
Test / flywayUser := ""
Test / flywayPassword := ""